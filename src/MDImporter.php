<?php

namespace CTR;

use \PhpOffice\PhpSpreadsheet;

class MDImporter
{
    protected $filesToProcess;
    protected $reader;
    protected $rollback;
    protected $directory;
    protected $db;
    protected $sheetIgnoreList;


    /**
     * MDImporter constructor.
     * @param \Zend_Db_Adapter_Abstract $db
     */
    public function __construct(\Zend_Db_Adapter_Abstract &$db)
    {
        $this->db = $db;
        $this->filesToProcess = [];
        $this->reader = new \PhpOffice\PhpSpreadsheet\Reader\Xlsx();
        $this->rollback = false;
        $this->directory = '';
        $this->setIgnoredSheets([]);
    }

    public function setIgnoredSheets(array $ignoreList){
        $this->sheetIgnoreList = $ignoreList;
    }

    /**
     * @param string $directory
     */
    public function findFilesToProcess(string $directory)
    {
        $this->directory = $directory;
        try {
            $this->filesToProcess = $metaDataFilesArray = \util\FileSystemUtil::getListOfFilesInDirectory($directory, array("xlsx", "xls"));
            $checkFilesInDir = $metaDataFilesArray == true ? 'Found directory...' : 'Could not find directory / no files in directory: ' . $directory;
            print($checkFilesInDir);
            $continue = true;
        } catch (Exception $e) {
            echo 'Could not find directory / no files in directory: ' . $directory . $e;
        }
    }


    /**
     * @param bool $removeExistingMetadata
     * @param bool $removeExistingStructure
     * @param bool|array $removeExistingStructure
     * @throws PhpSpreadsheet\Calculation\Exception
     * @throws \Zend_Db_Adapter_Exception
     */
    public function processMetadata(bool $removeExistingMetadata = true, bool $removeExistingStructure = false,$languagesIncluded=false)
    {
        $this->db->beginTransaction();
        if ($removeExistingMetadata) {
            $this->clearExistingForms($removeExistingStructure);
        }
        $acounter = 0;
        $continue = true;
        foreach ($this->filesToProcess as $metaDataFile) {
            try {
                echo "\n" . '++++  ' . $metaDataFile . '  ++++' . "\n";
                if (substr($metaDataFile, 0, 1) != '~') {
                    $objPHPExcel = $this->reader->load($this->directory . $metaDataFile);
                    foreach ($objPHPExcel->getWorksheetIterator() as $worksheet) {
                        if ($continue) {
                            echo ++$acounter . ". ";
                            echo str_pad($worksheet->getTitle(), 40, ".", STR_PAD_RIGHT);
                            if(in_array($worksheet->getTitle(),$this->sheetIgnoreList)){
                                echo "Ignored\n";
                            }else{
                                //iterate through all rows of spreadsheet and generate a data array which we can then evaluate
                                $rowIterator = $worksheet->getRowIterator(1);
                                $rawRow = array();
                                $informationTextCount = 0;
                                $rowc = 0;
                                foreach ($rowIterator as $row) {
                                    $cellIterator = $row->getCellIterator();
                                    $cellIterator->setIterateOnlyExistingCells(false); // Loop all cells, even if it is not set
                                    $rowIndex = $row->getRowIndex();
                                    foreach ($cellIterator as $cell) {
                                        if (\PhpOffice\PhpSpreadsheet\Shared\Date::isDateTime($cell)) {
                                            $rawRow[$rowIndex][$cell->getColumn()] = trim($cell->getCalculatedValue());
                                            /*
                                            var_dump($cell->getRawValue());
                                            $rawRow[$rowIndex][$cell->getColumn()] = PHPExcel_Shared_Date::ExcelToPHP($cell->getCalculatedValue());
                                            $datetime = new DateTime();
                                            $datetime->setTimestamp(PHPExcel_Shared_Date::ExcelToPHP($cell->getCalculatedValue()));
                                            $date = $datetime->format("d/m/Y");
                                            if (substr($date, -4, 4) == "1969") {
                                                $rawRow[$rowIndex][$cell->getColumn()] = "09/09/9999";
                                            } else {
                                                $rawRow[$rowIndex][$cell->getColumn()] = $date;
                                            }*/
                                        } else {
                                            $rawRow[$rowIndex][$cell->getColumn()] = trim($cell->getCalculatedValue());
                                        }
                                    }
                                }
                                //check the languages included in the metadata


                                //language columns are those after column W on standard template so compare titles and then move on from there
                                $languageColumnsFound = array_slice($rawRow[4],23, count($rawRow[4]));
                                $languagesToUse = [];
                                if(count($languageColumnsFound) > 0) {
                                    foreach ($languageColumnsFound as $langCol) {
                                        if (in_array(strtoupper($langCol),$languagesIncluded) != false) {
                                            $languagesToUse[] = strtoupper($langCol);
                                        }
                                    }
                                }
                                /*
                                 * now we have a list of all the meta data rows.
                                 * figure out what they contain, column labels are ABC etc
                                 * with each row of data from the spreadsheet
                                 */
                                $i = 0;
                                $topline = 1;
                                $startSet = false;
                                $order = 1;
                                $crf = "";
                                $currentPage = false;
                                for ($i = 1; $i <= count($rawRow); $i++) {//array is offset by 1 as there is no element 0
                                    if ($rawRow[$i]['A'] != 'ShortForm Title' && $startSet == false) {
                                        $topline++;
                                    } else {
                                        if ($rawRow[$i]['A'] == "") {
                                            if ($startSet == false) {
                                                $i++;
                                                $topline++;
                                            }
                                        }
                                        $startSet = true;
                                    }
                                    $insertArray = array();

                                    if ($i > $topline) {

                                        if ($topline == $i - 1) {
                                            //remove any whitespace in the form name description (this allows us to use it as the unique reference for the form
                                            $formTitle = str_replace(" ", "", $rawRow[$i]['A']);
                                            $currentPage = $this->addPage($formTitle, $formTitle);
                                        }
                                        if ($formTitle == "") {
                                            echo " - no data inserted - ";
                                            break;
                                        }
                                        $insertArray['formID'] = $formTitle;

                                        //start by adding a first page to the formMetadataPage table
                                        if(in_array(strtoupper($rawRow[$i]['C']),['PAGE BREAK','NEW PAGE']) || in_array(strtoupper($rawRow[$i]['D']),['PAGE BREAK','NEW PAGE'])){
                                            $currentPage = $this->addPage($formTitle, $rawRow[$i]['C']);
                                        }

                                        $insertArray['pageId'] = $currentPage;
                                        $insertArray['questionID'] = trim($rawRow[$i]['B']);

                                        //question number has been removed from spreadsheet template
                                        //$insertArray['questionNumber'] = trim($array_data[$i]['D']);
                                        $descriptions = $this->getDescriptionsFromRow($rawRow[$i],$languagesToUse);
                                        $insertArray = array_merge($insertArray,$descriptions);


                                        if (strtolower($rawRow[$i]['D']) == "tick box") {
                                            $rawRow[$i]['D'] = "tickbox";
                                        }

                                        $insertArray['type'] = strtoupper($rawRow[$i]['D']);

                                        //deal with instructions and information
                                        if (in_array(strtoupper($rawRow[$i]['D']), ['INSTRUCTION',
                                                'BLOCK',
                                                'INFORMATION',
                                                '[INSTRUCTION]',
                                                '[INFORMATION]'])
                                            && $insertArray['questionID'] == "") {
                                            $insertArray['questionID'] = "INFORMATION_" . $informationTextCount;
                                            $insertArray['type'] = "INFORMATION";
                                            $insertArray['description'] = preg_replace("/\n/","<br>",$insertArray['description']);

                                            $informationTextCount++;
                                        }

                                        if (is_numeric($rawRow[$i]['H'])) {
                                            $insertArray['maxLength'] = $rawRow[$i]['H'];
                                        }
                                        $insertArray['ordering'] = $order;

                                        $insertArray['value'] = trim($rawRow[$i]['E']);
                                        $insertArray['defaultValue'] = $rawRow[$i]['I'];
                                        $insertArray['mask'] = $rawRow[$i]['J'];

                                        $insertArray['skippable'] = ($rawRow[$i]['K'] == "1" || $rawRow[$i]['K'] == "Y") ? true : false;

                                        /**
                                         * Mandatory field bit setting: if field Y then set mandatory bit to 1 else to 0
                                         */
                                        //$insertArray['mandatory'] = (strtoupper($array_data[$i]['G']) == "Y") ? true : false;
                                        // default blanks to mandatory = Y
                                        if (strtoupper($rawRow[$i]['G']) == "Y") {
                                            $insertArray['mandatory'] = true;
                                        } else if (strtoupper($rawRow[$i]['G']) == "") {
                                            $insertArray['mandatory'] = true;
                                        } else if (strtoupper($rawRow[$i]['G']) == "N") {
                                            $insertArray['mandatory'] = false;
                                        }
                                        $parentOptionID = false;

                                        $optiontypes = array("category", "checkbox", "tickbox");
                                        if (in_array(strtolower($rawRow[$i]['D']), $optiontypes)) {
                                            $j = 0;
                                            $insertArray['value'] = '';
                                            // foreach of the subsequent array elements until the next $row e != "" do this:
                                            $catContinue = true;
                                            while ($catContinue) {

                                                $catContinue = false;
                                                $insertOptArray = array();
                                                $insertOptArray['formID'] = $insertArray['formID'];
                                                $insertOptArray['questionID'] = $insertArray['questionID'];
                                                $labelParts = array();
                                                if (stristr($rawRow[$i]['E'], "=")) {
                                                    $labelParts = explode("=", $rawRow[$i]['E']);
                                                    if ($rawRow['D'] == "") {
                                                        $insertArray['value'] = trim($labelParts[0]);
                                                    }
                                                } else {
                                                    $labelParts[1] = $rawRow[$i]['F'];
                                                }
                                                $optionRowTranslations = $this->getOptionDescriptionsFromRow($rawRow[$i],$languagesToUse);
                                                $insertOptArray = array_merge($insertOptArray, $optionRowTranslations);
                                                //$labelParts[1] = ucfirst($labelParts[1]);
                                                //$insertOptArray['description'] = trim($labelParts[1]);

                                                $insertOptArray['ordering'] = ++$j;
                                                $insertOptArray['value'] = $rawRow[$i]['E'];

                                                if ($parentOptionID) {
                                                    $insertOptArray['parentOptionID'] = $parentOptionID;
                                                }

                                                if (strtolower($rawRow[$i]['D']) == "subcategory") {
                                                    $insertOptArray['hasChildren'] = 1;
                                                    $parentOptionID = $this->db->lastInsertId('formMetadataOptions', 'optionID');
                                                } else {
                                                    $insertOptArray['hasChildren'] = 0;
                                                }


                                                if ($insertOptArray['value'] != "" || strtolower($rawRow[$i]['D']) == "subcategory") {
                                                    $this->db->insert("formMetadataOptions", $insertOptArray);
                                                }

                                                $i++;
                                                if (($rawRow[$i]['D'] == "" || strtolower($rawRow[$i]['D']) == "subcategory") && $rawRow[$i]['E'] !== "") {
                                                    $catContinue = true;
                                                } else if (strtolower($rawRow[$i]['D']) == "subcategory" && $rawRow[$i]['E'] == "") {
                                                    $catContinue = true;
                                                } else {
                                                    $catContinue = false;
                                                    $i--;
                                                }
                                                if ($rawRow[$i]['D'] == "" && $rawRow[$i]['E'] == "" && $rawRow[$i]['B'] == "") {
                                                    $catContinue = false;
                                                }
                                            }
                                            //store up options for inserts in subsequent rows.
                                        } else if (strtolower($rawRow[$i]['D']) == "subcategory") {

                                        } else {
                                            $insertOptArray = array();
                                        }
                                        //insert the data if it has a type (without type it can't be rendered)
                                        if ($insertArray['type'] != "") {
                                            if (strlen($insertArray['questionID']) > 0) {
                                                $this->db->insert("formMetadata", $insertArray);
                                            } else {
                                                echo "|questionID not detected: " . $insertArray['questionID'] . "(" . $i . " )|";
                                            }
                                        } else {
                                            //don't insert the section titles
                                            echo "|Typeless question detected: " . $insertArray['questionID'] . "(" . $i . " )|";
                                        }

                                    }
                                    $order++;
                                }
                                echo "(" . ($i - 1 - $topline) . " rows examined)\n";

                                /*
                                 *
                                 * Create basic form architecture if it doesn't exist already
                                 * 1. Create a top level block to contain all questions
                                 * 2. Insert all questions into block from the metadata, creating the appropriate formRowComponents
                                 * 3. Insert the text fields as required
                                 *
                                 */
                                $formBlockExistenceCheck = $this->db->fetchOne("select count(*) from formBlocks where formID = ? ", $formTitle);
                                if ($formBlockExistenceCheck == 0) {
                                    //insert new formblocks for new form
                                    $this->db->insert("formBlocks", ['formID' => $formTitle, 'title' => $formTitle, 'ordering' => 1, 'type' => 'block', 'parent' => 0, 'repeats' => 0, 'hasChildren' => 0]);
                                    // this is the formblock to use for all subsequent insertions in formRowComponent/formRow
                                    $blockID = $this->db->lastInsertId();
                                    //add all metadata into formBlock
                                    $metadataObjects = $this->db->fetchAll("select * from formMetadata where formID=?", $formTitle);
                                    foreach ($metadataObjects as $md) {
                                        $this->db->insert("formRow", ['blockID' => $blockID, 'ordering' => $md->ordering, 'rowType' => 'simple']);
                                        $rowId = $this->db->lastInsertId();
                                        $this->db->insert("formRowComponent", ['blockID' => $blockID, 'rowId' => $rowId, 'componentData' => $md->questionID, 'associatedMetadataQuestionID' => $md->questionID, 'associatedMetadataFormID' => $formTitle]);
                                    }
                                }
                                //$continue = false;
                            }
                        }
                    }
                } else {
                    echo "temp file - skipping";
                }
            } catch (Exception $e) {
                print_r($e);
                $this->rollback = true;
                #break;
            }
        }

        if ($this->rollback == true) {
            $this->rollback();
        } else {
            $this->commit();
        }

    }

    protected function getDescriptionsFromRow($row,$languagesIncluded=false){
        $insertArray = [];
        //master translation
        $insertArray['description'] = $row['C'];
        //other translations
        if($languagesIncluded){
            $startCol = 24; //column X
            foreach($languagesIncluded as $languageID) {
                $startCol + 2;
                $langCol = \PhpOffice\PhpSpreadsheet\Cell\Coordinate::stringFromColumnIndex($startCol);
                $insertArray['description'.$languageID] = $row[$langCol];
                $startCol = $startCol + 2;
            }
        }
        return $insertArray;

    }

    protected function getOptionDescriptionsFromRow($row,$languagesIncluded=false){
        $array = [];
        //master translation
        $labelParts[1] = ucfirst($row['F']);
        $array['description'] = $row['F'];
        //other translations
        if($languagesIncluded){
            $startCol = 25; //column X
            foreach($languagesIncluded as $languageID) {
                $startCol + 2;
                $langCol = \PhpOffice\PhpSpreadsheet\Cell\Coordinate::stringFromColumnIndex($startCol);
                if(preg_match("/^[a-z]/",$row[$langCol])){
                    $row[$langCol] = ucfirst($row[$langCol]);
                }
                $array['description'.$languageID] = $row[$langCol];
                $startCol = $startCol + 2;
            }
        }
        return $array;

    }


    public function clearExistingForms(bool $removeStructure = false)
    {
        $this->db->delete("formMetaData");
        $this->db->delete("formMetaDataOptions");
        if ($removeStructure == true) {
            $this->db->delete("formBlocks");
            $this->db->delete("formRow");
            $this->db->delete("formRowComponent");
            $this->db->delete('formMetadataPage');
        }
    }

    protected function addPage($formId,$pageTitle,$pageKey=false){
        $lastPage = $this->db->fetchOne("select top 1 pageOrder from formMetadataPage where formID = ? order by pageOrder desc",$formId);
        if($lastPage) {
            $nextOrder = $lastPage + 1;
        }else{
            $nextOrder = 1;
        }
        if(!$pageKey){
            $pageKey=$nextOrder;
        }

        $this->db->insert("formMetadataPage",['formID'=>$formId,'pageOrder'=>$nextOrder,'pageActive'=>1,'pageTitle'=>$pageTitle,'pageKey'=>$pageKey]);

        return $this->db->lastInsertId();
    }

    public function rollback()
    {
        $this->db->rollback();
    }

    public function commit()
    {
        $this->db->commit();
    }

}
