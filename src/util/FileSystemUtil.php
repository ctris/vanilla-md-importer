<?php

/**
 * Created by PhpStorm.
 * Class with util functions that be used in other classes
 * User: scmfq
 * Date: 06/03/2017
 * Time: 11:21
 */
namespace util;

class FileSystemUtil {
    /**
     * @param $dir string Absolute directory path for which we want to obtain the list of file on it
     * @return array with the names of the files in the directory received as parameter
     */
    public static function getListOfFilesInDirectory($dir, $arrExtension = array()){
        $files = array();
        if (is_dir($dir)) {
            $files = array_values(array_filter(scandir($dir), function ($file) use ($dir, $arrExtension) {
                if (!is_dir($dir . "/" . $file)) {
                    $file_parts = pathinfo($file);
                    if (count($arrExtension) == 0 || in_array(strtolower($file_parts["extension"]), $arrExtension)) {
                        return true;
                    } else {
                        return false;
                    }
                } else {
                    return false;
                }
            }));
        }
        return $files;
    }

    public static function getExtension($fileName) {
        $array = explode('.', $fileName);
        $extension = end($array);
        return $extension ? strtolower($extension) : false;
    }

    public static function createFolderStructure($filePath){
        $pathInfo = pathinfo($filePath);
        if (!is_dir($pathInfo['dirname'])) {
            mkdir($pathInfo['dirname'], 0777, true);
        }
    }
}