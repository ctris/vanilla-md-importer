create table forms
(
    formID varchar(16) not null
        constraint PK__forms__51BCB7CB0F975522
            primary key,
    name varchar(50),
    description varchar(255),
    versionid nvarchar(10),
    active int constraint DF__forms__active__286302EC default 1,
    maxVersions int constraint DF__forms__maxVersio__29572725 default 999,
    formOrdering int,
    formType nvarchar(50),
    timepoint nvarchar(50),
    minAge int,
    maxAge int,
    dataEncrypted bit,
    armLimited int,
    parentFormID nvarchar(16),
    paginated int,
    numberedQuestions int,
    nameCY varchar(50),
    global int,
    languagesAvailable varchar(255)
)
go


create table formMetadata
(
    formID nvarchar(50) not null,
    questionID nvarchar(50) not null,
    description nvarchar(max) not null,
    defaultValue nvarchar(50) constraint DF__formMetad__defau__1A14E395 default '-9',
    value nvarchar(255),
    type nvarchar(50),
    maxLength nvarchar(50) constraint DF__formMetad__maxLe__1B0907CE default 999,
    ordering int,
    mandatory bit not null,
    skippable bit,
    skipAntecedent nvarchar(50),
    skipCondition nvarchar(50),
    skipDescription nvarchar(200),
    questionNumber nvarchar(10),
    descriptionCY nvarchar(max),
    mask varchar(255),
    pageId int
        constraint formMetadata_formMetadataPage_id_fk
            references formMetadataPage,
    descriptionDE nvarchar(max),
    descriptionPL nvarchar(max),
    descriptionES nvarchar(max),
    constraint PK__formMeta__679F3A820CBAE877
        primary key (formID, questionID)
)
go

create table formMetadataOptions
(
    optionID int identity
        constraint PK__formMeta__3D5DC3C1145C0A3F
            primary key,
    formID varchar(16) not null,
    questionID varchar(50) not null,
    description nvarchar(max),
    value varchar(255),
    ordering int,
    parentOptionID int,
    hasChildren int,
    descriptionCY nvarchar(510),
    descriptionDE nvarchar(max),
    descriptionPL nvarchar(max),
    descriptionES nvarchar(max)
)
go

create table formMetadataPage
(
    id int identity
        constraint metadataFormPage_pk
            primary key nonclustered,
    formID varchar(255) not null,
    pageOrder int not null,
    pageTitle varchar(255),
    pageActive int,
    pageKey int
)
go

create table formBlocks
(
    formID nvarchar(50) not null,
    blockID int identity,
    title nvarchar(255),
    description text,
    ordering int,
    parent int,
    repeats int,
    type varchar(50),
    hasChildren int,
    matrixID int,
    titleCY nvarchar(255),
    descriptionCY text,
    skipID nvarchar(50),
    constraint PK_formBlocks
        primary key (formID, blockID)
)
go

create table formRow
(
    rowID int identity,
    blockID int not null,
    ordering int,
    rowType nvarchar(50),
    constraint PK_formRow
        primary key (rowID, blockID)
)
go

create table formRowComponent
(
    componentID int identity,
    rowID int not null,
    blockID int not null,
    type varchar(50),
    componentData ntext,
    ordering int,
    associatedMetadataQuestionID nvarchar(50),
    associatedMetadataFormID nvarchar(50),
    labeldata ntext,
    styleData text,
    matrixColumns int,
    labeldataCY ntext,
    constraint PK_formRowComponent
        primary key (componentID, rowID, blockID)
)
go

