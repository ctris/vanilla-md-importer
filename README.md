# CTR\MDImporter README #

CTR\MDImporter readme file

## What is this repository for? 

MDImporter is a CTR tool to process metadata from clinical studies to power rendered forms on clinical databases.

## Version history 
* v0.1 - unstable - check back for stable release in the future
* v0.2 - unstable - added language options and paging information new option to pass in available languages on processing

## Including the library
### 1. Add the ctr library repository

Add the ctr library repository to your `composer.json` file as follows:
```json
    "repositories": [{
        "type": "git-bitbucket",
        "url": "https://bitbucket.org/mtevp/vanilla-md-importer/"
    }]
```

### 2. Require the library

Add `ctr/md-importer` to your composer.json either using the composer app 
  
    require ctr/md-importer:[v0.1|dev-master]

 
or manually add:
```json
    "require": {
        "ctr/md-importer": "dev-master"
    }
```
 
### 3. Ensuring your data schema is prepared

This MD importer requires specific SQL schema to be in available on the target database or it will fail in a horrible way.<br>
Check the `/schema/db/` folder for the SQL table structure required
* * * *

## Basic usage:

```php
require_once(__DIR__ . "/vendor/autoload.php");
$metaDataDir = "../metadata/";
$importer = new CTR\MDImporter([$dbAdapter]);
$importer->findFilesToProcess($metaDataDir);
$importer->processMetadata(true,true,['LANG2','LANG3',...]);
```

